<?php
//load settings
$gallery_settings = unserialize(base64_decode(get_post_meta( $post->ID, 'awl_ig_settings_'.$post->ID, true)));
//print_r($gallery_settings);
$image_gallery_id = $post->ID;

$col_large_desktops = $gallery_settings['col_large_desktops'];
$col_desktops = $gallery_settings['col_desktops'];
$col_tablets = $gallery_settings['col_tablets'];
$col_phones = $gallery_settings['col_phones'];
?>
<style>
.nig_settings {
	padding: 8px 0px 8px 8px !important;
	margin: 10px 10px 4px 0px !important;
	border-bottom: 1px dashed #00A0D2 !important;
}
.nig_settings label {
	font-size: 16px !important;
}
.be-right {
	float: right;
	text-align: right;
	text-decoration: none;
}
</style>
<p class="nig_settings">
	<label>Gallery Thumbnail Size</label></br>
	<?php if(isset($gallery_settings['gal_thumb_size'])) $gal_thumb_size = $gallery_settings['gal_thumb_size']; else $gal_thumb_size = "thumbnail"; ?>
	<select id="gal_thumb_size" name="gal_thumb_size" class="form-control">
		<option value="thumbnail" <?php if($gal_thumb_size == "thumbnail") echo "selected=selected"; ?>>Thumbnail – 150 × 150</option>
		<option value="medium" <?php if($gal_thumb_size == "medium") echo "selected=selected"; ?>>Medium – 300 × 169</option>
		<option value="large" <?php if($gal_thumb_size == "large") echo "selected=selected"; ?>>Large – 840 × 473</option>
		<option value="full" <?php if($gal_thumb_size == "full") echo "selected=selected"; ?>>Full Size – 1280 × 720</option>
	</select><br>
	<?php _e('Select gallery thumnails size to display into gallery', IGP_TXTDM); ?><a class="be-right" href="#">Go To Top</a>
</p>

<p class="nig_settings">
	<label>Columns On Large Desktops</label></br>
	<?php if(isset($gallery_settings['col_large_desktops'])) $col_large_desktops = $gallery_settings['col_large_desktops']; else $col_large_desktops = "col-lg-2"; ?>
	<select id="col_large_desktops" name="col_large_desktops" class="form-control">
		<option value="col-lg-12" <?php if($col_large_desktops == "col-lg-12") echo "selected=selected"; ?>>1 Column</option>
		<option value="col-lg-6" <?php if($col_large_desktops == "col-lg-6") echo "selected=selected"; ?>>2 Column</option>
		<option value="col-lg-4" <?php if($col_large_desktops == "col-lg-4") echo "selected=selected"; ?>>3 Column</option>
		<option value="col-lg-3" <?php if($col_large_desktops == "col-lg-3") echo "selected=selected"; ?>>4 Column</option>
		<option value="col-lg-2" <?php if($col_large_desktops == "col-lg-2") echo "selected=selected"; ?>>6 Column</option>
		<option value="col-lg-1" <?php if($col_large_desktops == "col-lg-1") echo "selected=selected"; ?>>12 Column</option>
	</select><br>
	<?php _e('Select gallery column layout for large desktop devices', IGP_TXTDM); ?><a class="be-right" href="#">Go To Top</a>
</p>

<p class="nig_settings">
	<label>Columns On Desktops</label></br>
	<?php if(isset($gallery_settings['col_desktops'])) $col_desktops = $gallery_settings['col_desktops']; else $col_desktops = "col-md-3"; ?>
	<select id="col_desktops" name="col_desktops" class="form-control">
		<option value="col-md-12" <?php if($col_desktops == "col-md-12") echo "selected=selected"; ?>>1 Column</option>
		<option value="col-md-6" <?php if($col_desktops == "col-md-6") echo "selected=selected"; ?>>2 Column</option>
		<option value="col-md-4" <?php if($col_desktops == "col-md-4") echo "selected=selected"; ?>>3 Column</option>
		<option value="col-md-3" <?php if($col_desktops == "col-md-3") echo "selected=selected"; ?>>4 Column</option>
		<option value="col-md-2" <?php if($col_desktops == "col-md-2") echo "selected=selected"; ?>>6 Column</option>
		<option value="col-md-1" <?php if($col_desktops == "col-md-1") echo "selected=selected"; ?>>12 Column</option>
	</select><br>
	<?php _e('Select gallery column layout for desktop devices', IGP_TXTDM); ?><a class="be-right" href="#">Go To Top</a>
</p>

<p class="nig_settings">
	<label>Columns On Tablets</label></br>
	<?php if(isset($gallery_settings['col_tablets'])) $col_tablets = $gallery_settings['col_tablets']; else $col_tablets = "col-sm-4"; ?>
	<select id="col_tablets" name="col_tablets" class="form-control">
		<option value="col-sm-12" <?php if($col_tablets == "col-sm-12") echo "selected=selected"; ?>>1 Column</option>
		<option value="col-sm-6" <?php if($col_tablets == "col-sm-12") echo "selected=selected"; ?>>2 Column</option>
		<option value="col-sm-4" <?php if($col_tablets == "col-sm-4") echo "selected=selected"; ?>>3 Column</option>
		<option value="col-sm-3" <?php if($col_tablets == "col-sm-3") echo "selected=selected"; ?>>4 Column</option>
		<option value="col-sm-2" <?php if($col_tablets == "col-sm-2") echo "selected=selected"; ?>>6 Column</option>
	</select><br>
	<?php _e('Select gallery column layout for tablet devices', IGP_TXTDM); ?><a class="be-right" href="#">Go To Top</a>
</p>

<p class="nig_settings">
	<label>Columns On Phones</label></br>
	<?php if(isset($gallery_settings['col_phones'])) $col_phones = $gallery_settings['col_phones']; else $col_phones = "col-xs-6"; ?>
	<select id="col_phones" name="col_phones" class="form-control">
		<option value="col-xs-12" <?php if($col_phones == "col-xs-12") echo "selected=selected"; ?>>1 Column</option>
		<option value="col-xs-6" <?php if($col_phones == "col-xs-6") echo "selected=selected"; ?>>2 Column</option>
		<option value="col-xs-4" <?php if($col_phones == "col-xs-4") echo "selected=selected"; ?>>3 Column</option>
		<option value="col-xs-3" <?php if($col_phones == "col-xs-3") echo "selected=selected"; ?>>4 Column</option>
	</select><br>
	<?php _e('Select gallery column layout for phone devices', IGP_TXTDM); ?><a class="be-right" href="#">Go To Top</a>
</p>

<p class="nig_settings">
	<label><?php _e('Light Box Style', IGP_TXTDM); ?></label></br>
	<?php if(isset($gallery_settings['light-box'])) $light_box = $gallery_settings['light-box']; else $light_box = 1; ?>
	<select name="light-box" id="light-box">	
		<option value="0" <?php if($light_box == 0) echo "selected=selected"; ?>>None</option>
		<option value="6" <?php if($light_box == 6) echo "selected=selected"; ?>>Bootstrap Light Box</option>
	</select><br>
	<?php _e('Select a light box style', IGP_TXTDM); ?><a class="be-right" href="#">Go To Top</a>
</p>

<p class="nig_settings">
	<label><?php _e('Image Hover Effect Type', IGP_TXTDM); ?></label></br>
	<?php if(isset($gallery_settings['image_hover_effect_type'])) $image_hover_effect_type = $gallery_settings['image_hover_effect_type']; else $image_hover_effect_type = "sg"; ?>
	<input type="radio" name="image_hover_effect_type" id="image_hover_effect_type" value="no" <?php if($image_hover_effect_type == "no") echo "checked=checked"; ?>>&nbsp;<?php _e('None', IGP_TXTDM); ?><br>
	<input type="radio" name="image_hover_effect_type" id="image_hover_effect_type" value="sg" <?php if($image_hover_effect_type == "sg") echo "checked=checked"; ?>>&nbsp;Shadow and Glow Transitions Effects<br>
	<?php _e('Select a image hover effect type', IGP_TXTDM); ?><a class="be-right" href="#">Go To Top</a>
</p>



<!-- 4 -->
<p class="he_four nig_settings">
	<label><?php _e('Image Hover Effects', IGP_TXTDM); ?></label><br>
	<?php if(isset($gallery_settings['image_hover_effect_four'])) $image_hover_effect_four = $gallery_settings['image_hover_effect_four']; else $image_hover_effect_four = "hvr-glow"; ?>
	<select name="image_hover_effect_four" id="image_hover_effect_four">
		<optgroup label="Shadow and Glow Transitions Effects" class="sg">
			<option value="hvr-grow-shadow" <?php if($image_hover_effect_four == "hvr-grow-shadow") echo "selected=selected"; ?>>Grow Shadow</option>
			<option value="hvr-float-shadow" <?php if($image_hover_effect_four == "hvr-float-shadow") echo "selected=selected"; ?>>Float Shadow</option>
			<option value="hvr-glow" <?php if($image_hover_effect_four == "hvr-glow") echo "selected=selected"; ?>>Glow</option>
			<option value="hvr-box-shadow-inset" <?php if($image_hover_effect_four == "hvr-box-shadow-inset") echo "selected=selected"; ?>>Box-Shadow-Inset</option>
			<option value="hvr-box-shadow-outset" <?php if($image_hover_effect_four == "hvr-box-shadow-outset") echo "selected=selected"; ?>>Box Shadow Outset</option>
		</optgroup>
	</select><br>
	<?php _e('Set an image hover effect on gallery', IGP_TXTDM); ?><a class="be-right" href="#">Go To Top</a>
</p>

<p class="nig_settings">
	<label><?php _e('Hide Thumbnails Spacing', IGP_TXTDM); ?></label><br>
	<?php if(isset($gallery_settings['no_spacing'])) $no_spacing = $gallery_settings['no_spacing']; else $no_spacing = 0; ?>
	<input type="radio" name="no_spacing" id="no_spacing" value="1" <?php if($no_spacing == 1) echo "checked=checked"; ?>>&nbsp;<?php _e('Yes', IGP_TXTDM); ?><br>
	<input type="radio" name="no_spacing" id="no_spacing" value="0" <?php if($no_spacing == 0) echo "checked=checked"; ?>>&nbsp;<?php _e('No', IGP_TXTDM); ?><br>
	<?php _e('Hide gap / margin / padding / spacing between gallery thumbnails', IGP_TXTDM); ?><a class="be-right" href="#">Go To Top</a>
</p>

<p class="nig_settings">
	<label><?php _e('Gallery Thumbnail Order', IGP_TXTDM); ?></label><br>
	<?php if(isset($gallery_settings['thumbnail_order'])) $thumbnail_order = $gallery_settings['thumbnail_order']; else $thumbnail_order = "ASC"; ?>
	<input type="radio" name="thumbnail_order" id="thumbnail_order" value="ASC" <?php if($thumbnail_order == "ASC") echo "checked=checked"; ?>>&nbsp;<?php _e('Oldest Thumbnails First', IGP_TXTDM); ?><br>
	<input type="radio" name="thumbnail_order" id="thumbnail_order" value="DESC" <?php if($thumbnail_order == "DESC") echo "checked=checked"; ?>>&nbsp;<?php _e('Newest Thumbnails First', IGP_TXTDM); ?><br>
	<input type="radio" name="thumbnail_order" id="thumbnail_order" value="RANDOM" <?php if($thumbnail_order == "RANDOM") echo "checked=checked"; ?>>&nbsp;<?php _e('Random / Suffled Thumbnails', IGP_TXTDM); ?><br>
	<?php _e('Set a order in which you want to display gallery thumnails', IGP_TXTDM); ?><a class="be-right" href="#">Go To Top</a>
</p>

<p class="nig_settings">
	<label><?php _e('Custom CSS', IGP_TXTDM); ?></label></br>
	<?php if(isset($gallery_settings['custom-css'])) $custom_css = $gallery_settings['custom-css']; else $custom_css = ""; ?>
	<textarea name="custom-css" id="custom-css" style="width: 100%; height: 120px;" placeholder="Type direct CSS code here. Don't use <style>...</style> tag."><?php echo $custom_css; ?></textarea><br>
	<?php _e('Apply own css on image gallery and dont use style tag', IGP_TXTDM); ?><a class="be-right" href="#">Go To Top</a>
</p>

<p class="">
	<h1><strong>Early Bird Offer:</strong> Upgrade To Premium Just In Half Price <strike>$15</strike> <strong>$7</strong></h1>
	<br>
	<a href="http://awplife.com/product/image-gallery-premium/" target="_blank" class="button button-primary button-hero load-customize hide-if-no-customize">Premium Version Details</a>
	<a href="http://demo.awplife.com/image-gallery-premium/" target="_blank" class="button button-primary button-hero load-customize hide-if-no-customize">Check Live Demo</a>
	<a href="http://demo.awplife.com/image-gallery-premium-admin-demo/" target="_blank" class="button button-primary button-hero load-customize hide-if-no-customize">Try Pro Version</a>
</p>

<input type="hidden" name="ig-settings" id="ig-settings" value="ig-save-settings">

<script>
var effect_type = jQuery('input[name="image_hover_effect_type"]:checked').val();
//alert(effect_type);
if(effect_type == "no") {
	jQuery('.he_one').hide();
	jQuery('.he_two').hide();
	jQuery('.he_three').hide();
	jQuery('.he_four').hide();
	jQuery('.he_five').hide();
	jQuery('.image-hover-color').show();
	jQuery('.title-bg-color').show();
}
if(effect_type == "sg") {
	jQuery('.he_one').hide();
	jQuery('.he_two').hide();
	jQuery('.he_three').hide();
	jQuery('.he_four').show();
	jQuery('.he_five').hide();
	jQuery('.title-bg-color').show();
	jQuery('.image-hover-color').show();
}

//on change effect
jQuery(document).ready(function() {
	jQuery('input[name="image_hover_effect_type"]').change(function(){
		var effect_type = jQuery('input[name="image_hover_effect_type"]:checked').val();
		if(effect_type == "no") {
			jQuery('.he_one').hide();
			jQuery('.he_two').hide();
			jQuery('.he_three').hide();
			jQuery('.he_four').hide();
			jQuery('.he_five').hide();
			jQuery('.title-bg-color').hide();
			jQuery('.image-hover-color').hide();
		}
		if(effect_type == "sg") {
			jQuery('.he_one').hide();
			jQuery('.he_two').hide();
			jQuery('.he_three').hide();
			jQuery('.he_four').show();
			jQuery('.he_five').hide();
			jQuery('.title-bg-color').show();
			jQuery('.image-hover-color').show();
		}
	});
});
// start pulse on page load
function pulseEff() {
   jQuery('#shortcode').fadeOut(600).fadeIn(600);
};
var Interval;
Interval = setInterval(pulseEff,1500);

// stop pulse
function pulseOff() {
	clearInterval(Interval);
}
// start pulse
function pulseStart() {
	Interval = setInterval(pulseEff,2000);
}
</script>