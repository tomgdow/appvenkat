=== Image Gallery ===
Contributors: awordpresslife
Tags: image, gallery, widget, photo, video, link, url, picture, lightbox, shortcode, image gallery, photo gallery, album gallery, picture gallery, ligthbox gallery, wordpress gallery, responsive gallery
Donate link: http://awplife.com/
Requires at least: 4.0
Tested up to: 4.6
Stable tag: 0.0.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Responsive Simple Beautiful Easy Powerful Image Gallery Plugin For WordPress

== Description ==

Responsive Simple Beautiful Easy Powerful WordPress Gallery Plugin With Light Box Style. Create gallery using custom post type and use gallery shortcode to publish your gallery anywhere in WordPress.

**How to use plugin?**

Download & install the plugin into your site. After successful installation of the plugin go to plugins "DOC" menu for help and instructions.

**Standard Features**

* Responsive Gallery
* Multiple Column Layouts
* Bootstrap 3.3.6 Based
* 4 Hover Effect 
* 1 Light Box Style
* Image Title
* Gallery in Post
* Gallery in Page
* Gallery in Widget
* Custom CSS

**Get Premium Version With More Features**

* Easy & Simple
* Responsive Gallery
* Grid Gallery
* Masonry Gallery
* Link Gallery
* Multiple Column Layouts
* Bootstrap 3.3.6 Based
* 30 Hover Effect 
* 5 Light Box Pop-up Style
* Image Title
* Gallery in Post
* Gallery in Page
* Gallery in Widget
* Seo Friendly Gallery
* Thumbnail Size & Quality Setting
* Navigation Error in Lightbox Preview
* Gallery Images Order Setting Like Ascending, Descending & Shuffle
* Simple & User-Friendly Custom Plugin Dashboard
* Create Unlimited Galleries With Unlimited Images
* Custom CSS
* Easy To Implement Anywhere Into WordPress
* Easily Customization
* Fast, Friendly & Quality Support

**Upgrade To Premium Plugin - <a href="http://awplife.com/product/image-gallery-premium/">Click Here</a>**

**Check Premium Plugin Demo - <a href="http://demo.awplife.com/image-gallery-premium/">Click Here</a>**

== Installation ==

Install  New Image Gallery either via the WordPress.org plugin directory or by uploading the files to your server.

After activating New Image Gallery plugin, go to plugin menu.

Login into WordPress admin dashboard. Go to menu: New Image Gallery --> Add Image Gallery

Create gallery & configure settings and save.

Copy shortcode and paste shortcode into any Page / Post. And view page for gallery output.

That's it. You're ready to go!

== Frequently Asked Questions ==

Have any queries?

Please post your question on plugin support forum

https://wordpress.org/support/plugin/new-image-gallery/

== Screenshots ==

1. Image Gallery On Website
2. Grid Gallery With Six Columns
3. Gallery Into Sidebar Widget
4. Widget Gallery With Multiple Columns Example
5. Grid Gallery Six Columns Without Spacing & Border
6. Grid Gallery With Spacing & Border
7. Grid Gallery Six Columns With Spacing & Border Same Size Thumbnails
8. Masonry Gallery Four Columns
9. Masonry Gallery Four Columns With Spacing & Border
10. Masonry Gallery Three Columns With Spacing & Border
11. Masonry Gallery Two Columns With Spacing & Border

== Changelog ==

Feature Enhancements: Version 0.0.6
* Enhancements: None
* Bug Fix: Spelling issues Fixed
* Additional changes: None

Feature Enhancements: Version 0.0.5
* Enhancements: None
* Bug Fix: Image Overlapping Fixed
* Additional changes: None

Feature Enhancements: Version 0.0.4
* Enhancements: None
* Bug Fix: None
* Additional changes: Working On Latest Version

Feature Enhancements: Version 0.0.3
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.0.2
* Enhancements: None
* Bug Fix: None
* Additional changes: Typos

Feature Enhancements: Version 0.0.1
* Enhancements: None
* Bug Fix: None
* Additional changes: None

== Upgrade Notice ==
This is an initial release. Start with version 0.0.1. and share your feedback <a href="https://wordpress.org/support/view/plugin-reviews/new-image-gallery/">here</a>.